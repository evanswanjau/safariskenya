<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>About Us</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Tours and Travel Agency">
    <link href="https://fonts.googleapis.com/css?family=Palanquin" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script type="text/javascript" src="../js/script.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <link rel="icon" type="image/png" href="../images/logo.png">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>


    <!-- Menu Section -->

    <section>
      <div class="top-menu">
        <ul class="left">
          <a href="../"><img class="white" src="../images/logo-white.png" alt="logo"><img class="black" src="../images/logo.png" alt="logo"></a>
        </ul>
        <ul class="links">
          <a href="../destinations"><li>destinations</li></a>
          <a href="../hotels"><li>hotels</li></a>
          <a href="../offers"><li>offers</li></a>
          <a href="../about"><li>about</li></a>
          <a href="../contacts"><li>contacts</li></a>
        </ul>
      </div>
    </section>






    <!-- About Welcome Section -->

    <section>
      <div class="welcome-to-about">
        <div class="text">
          <h1 data-aos='fade-up' data-aos-duration="1000">We provide a great travel experience</h1>
        </div>
      </div>
    </section>



    <!-- About Us Section -->
    <section>
      <div class="container about-us">
        <div class="row">
          <div class="col-sm-6">
            <img src="../images/about.jpg" alt="about us">
          </div>
          <div class="col-sm-6">
            <h3>About Us</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
              ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
              laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </div>
      </div>
    </section>








    <!-- Footer Section -->

    <div class="container-fluid footer">
      <div class="row">
        <div class="col-sm-4 links">
          <ul>
            <a href="../destinations"><li>destinations</li></a>
            <a href="../hotels"><li>hotels</li></a>
            <a href="../offers"><li>offers</li></a>
            <a href="../about"><li>about</li></a>
            <a href="../contacts"><li>contacts</li></a>
          </ul>
        </div>
        <div class="col-sm-4 social">
          <ul>
            <a href="#" target="_blank"><li><i class="fab fa-facebook"></i></li></a>
            <a href="#" target="_blank"><li><i class="fab fa-twitter"></i></li></a>
            <a href="#" target="_blank"><li><i class="fab fa-instagram"></i></li></a>
          </ul>
        </div>
        <div class="col-sm-4 subscribe">
          <form class="ui-form" action="" method="post">
            <p>Join our emailing list and be the first to receive our amazing offers</p>
            <input type="email" name="email" placeholder="email"><br>
            <input type="submit" name="subscribe" value="subscribers">
          </form>
        </div>
      </div>
    </div>



  </body>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>AOS.init();</script>
</html>
