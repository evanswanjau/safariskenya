<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Offers</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Tours and Travel Agency">
    <link href="https://fonts.googleapis.com/css?family=Palanquin" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script type="text/javascript" src="../js/script.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <link rel="icon" type="image/png" href="../images/logo.png">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>


    <!-- Menu Section -->

    <section>
      <div class="top-menu">
        <ul class="left">
          <a href="../"><img class="white" src="../images/logo-white.png" alt="logo"><img class="black" src="../images/logo.png" alt="logo"></a>
        </ul>
        <ul class="links">
          <a href="../destinations"><li>destinations</li></a>
          <a href="../hotels"><li>hotels</li></a>
          <a href="../offers"><li>offers</li></a>
          <a href="../about"><li>about</li></a>
          <a href="../contacts"><li>contacts</li></a>
        </ul>
      </div>
    </section>

    <!-- Holds the image display section -->

    <section>
      <div class="background-holder">
        <div class="close-it">
          <i class="fa fa-times"></i>
        </div>
        <div class="item-holder">
          <span id="imageOffer"></span>
        </div>
      </div>
    </section>




    <!-- Offers Welcome Sections -->

    <section>
      <div class="welcome-to-offers">
        <div class="text">
          <h1 data-aos='fade-up' data-aos-duration="1000"><span>We have great offers waiting for you this holiday</span></h1>
        </div>
      </div>
    </section>



    <!-- Image Offers -->

    <div class="container-fluid offers">
      <div class="row">
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/1.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/1.jpg">
        </div>
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/2.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/2.jpg">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/3.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/3.jpg">
        </div>
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/4.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/4.jpg">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/5.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/5.jpg">
        </div>
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/6.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/6.jpg">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/7.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/7.jpg">
        </div>
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/8.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/8.jpg">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/9.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/11.jpg">
        </div>
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/10.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/12.jpg">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/11.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/9.jpg">
        </div>
        <div class="col-sm-6 offer-holder" onclick="displayImage('../images/offers/12.jpg')" data-aos='fade-up' data-aos-duration="1000">
          <img src="../images/offers/10.jpg">
        </div>
      </div>
    </div>



    <!-- Footer Section -->

    <div class="container-fluid footer">
      <div class="row">
        <div class="col-sm-4 links">
          <ul>
            <a href="destinations"><li>destinations</li></a>
            <a href="hotels"><li>hotels</li></a>
            <a href="offers"><li>offers</li></a>
            <a href="about"><li>about</li></a>
            <a href="contacts"><li>contacts</li></a>
          </ul>
        </div>
        <div class="col-sm-4 social">
          <ul>
            <a href="#" target="_blank"><li><i class="fab fa-facebook"></i></li></a>
            <a href="#" target="_blank"><li><i class="fab fa-twitter"></i></li></a>
            <a href="#" target="_blank"><li><i class="fab fa-instagram"></i></li></a>
          </ul>
        </div>
        <div class="col-sm-4 subscribe">
          <form class="ui-form" action="" method="post">
            <p>Join our emailing list and be the first to receive our amazing offers</p>
            <input type="email" name="email" placeholder="email"><br>
            <input type="submit" name="subscribe" value="subscribers">
          </form>
        </div>
      </div>
    </div>


  </body>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>AOS.init();</script>
</html>
