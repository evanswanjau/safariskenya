<?php


/**
 * CRUD CLASS
 */

class CrudFunctionality{



  # method to add items to db
  public function addItems($assoc, $table){

    global $conn;

    $return_value = null; # default return value

    $columns = array();

    $values = array();



    foreach ($assoc as $key => $value) {

      $columns[] = $key;

      $values[] = "'".$value."'";

    }




    # convert columns and data into sql format
    $column_data = implode($columns, ', ');

    $value_data = implode($values, ', ');



    #Inserting the user's data into our database
    $sql = "INSERT INTO $table ($column_data) VALUES ($value_data)";

    if ($conn->query($sql) === TRUE) {

      $return_value = true;

    }else {

      $return_value = "Error: " . $sql . "<br>" . $conn->error;

    }

    return $return_value;

  }




  # method to get items from db
  public function getData($item, $table, $field=0, $value=0, $order=0, $design=0){

    global $conn;

    $assoc = array();

    $return_value = null; # default return value



    # get items from db

    $sql = "SELECT $item FROM $table WHERE $field = '$value' ORDER BY '$order' '$design'";

    $result = $conn->query($sql);


    if ($result->num_rows > 0) {

      while($row = $result->fetch_assoc()) {

        $assoc[] = $row;

      }

      $return_value = $assoc;

    }else {

      $return_value = 'error';

    }



    return $return_value;

  }







  # method to update items in db

  public function updateData($table, $column, $value, $field, $clause){


    global $conn;

    $assoc = array();

    $return_value = null; # default return value

    # update item in db

    $sql = "UPDATE $table SET $column = '$value' WHERE $field = '$clause'";

    $result = $conn->query($sql);


    if ($conn->query($sql) === TRUE) {

      $return_value = true;

    }else {

      $return_value = "Error: " . $sql . "<br>" . $conn->error;

    }

    return $return_value;

  }





  # method to deelte item from db

  public function deleteItem($table, $field, $clause){

    global $conn;

    $return_value = null;

    $sql = "DELETE FROM $table WHERE $field = '$clause'";


    if ($conn->query($sql) === TRUE) {

      $return_value = true;

    }else {

      $return_value =  "Error: " . $sql . "<br>" . $conn->error;

    }

  }



}







/*
--------------------------------------------
HOTEL FUNCTIONALITY
--------------------------------------------
*/


function getHotels(){

  $hotel = new CrudFunctionality;

  $hotel_data = $hotel->getData('*', 'hotels');


  # display the hotels

  for ($i=0; $i < count($hotel_data); $i++) {

    echo "
    <div class='col-sm-3 hotel'>
      <div class='image' style='background: url(../images/hotel-images/".$hotel_data[$i]['image'].") no-repeat center center; background-size: cover;'>
      <div class='text'>
        <h3 class='cap'>".$hotel_data[$i]['name']."<h3>
        <a href='../contacts' class='button'>book now</a>
      </div>
      </div>
    </div>";

  }


}

function getDestinations(){

  $hotel = new CrudFunctionality;

  $hotel_data = $hotel->getData('*', 'destinations');


  # display the hotels

  for ($i=0; $i < count($hotel_data); $i++) {

    echo "
    <div class='col-sm-3 hotel'>
      <div class='image' style='background: url(../images/destinations/".$hotel_data[$i]['image'].") no-repeat center center; background-size: cover;'>
      <div class='text'>
        <h3 class='cap'>".$hotel_data[$i]['name']."<h3>
        <a href='../contacts' class='button'>travel</a>
      </div>
      </div>
    </div>";

  }


}



 ?>
