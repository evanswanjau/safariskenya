<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Contacts</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Tours and Travel Agency">
    <link href="https://fonts.googleapis.com/css?family=Palanquin" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script type="text/javascript" src="../js/script.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <link rel="icon" type="image/png" href="../images/logo.png">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>


    <!-- Menu Section -->

    <section>
      <div class="top-menu">
        <ul class="left">
          <a href="../"><img class="white" src="../images/logo-white.png" alt="logo"><img class="black" src="../images/logo.png" alt="logo"></a>
        </ul>
        <ul class="links">
          <a href="../destinations"><li>destinations</li></a>
          <a href="../hotels"><li>hotels</li></a>
          <a href="../offers"><li>offers</li></a>
          <a href="../about"><li>about</li></a>
          <a href="../contacts"><li>contacts</li></a>
        </ul>
      </div>
    </section>



    <!-- Contacts -->

    <section>
      <div class="contacts-page">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 contact">
              <h3>Contact</h3>
              <ul>
                <li><i class="fa fa-phone left"></i>0720 707 005</li>
                <li><i class="fa fa-phone left"></i>0723 853 204</li>
                <li><i class="fa fa-envelope left"></i>info@safariskenya.co.ke</li>
                <li><i class="fa fa-envelope left"></i>bookings@safariskenya.co.ke</li>
                <li><i class="fa fa-map-marker-alt left"></i>Thika road, Garden estate road, Garden meadows restaurant, Opposite Bible School</li>
              </ul>
            </div>
            <div class="col-sm-6 form">
              <h3>Send us a message</h3>
              <form class="ui-form" action="" method="post">
                <input type="text" name="name" placeholder="full name">
                <input type="text" name="phone" placeholder="phone number"><br><br>
                <input type="email" name="email" placeholder="email"><br><br>
                <textarea name="name" rows="4" cols="80" placeholder="send us a message"></textarea><br><br>
                <input type="submit" name="send-message" value="submit">
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>




    <!-- Footer Section -->

    <div class="container-fluid footer">
      <div class="row">
        <div class="col-sm-4 links">
          <ul>
            <a href="../destinations"><li>destinations</li></a>
            <a href="../hotels"><li>hotels</li></a>
            <a href="../offers"><li>offers</li></a>
            <a href="../about"><li>about</li></a>
            <a href="../contacts"><li>contacts</li></a>
          </ul>
        </div>
        <div class="col-sm-4 social">
          <ul>
            <a href="#" target="_blank"><li><i class="fab fa-facebook"></i></li></a>
            <a href="#" target="_blank"><li><i class="fab fa-twitter"></i></li></a>
            <a href="#" target="_blank"><li><i class="fab fa-instagram"></i></li></a>
          </ul>
        </div>
        <div class="col-sm-4 subscribe">
          <form class="ui-form" action="" method="post">
            <p>Join our emailing list and be the first to receive our amazing offers</p>
            <input type="email" name="email" placeholder="email"><br>
            <input type="submit" name="subscribe" value="subscribers">
          </form>
        </div>
      </div>
    </div>



  </body>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>AOS.init();</script>
</html>
