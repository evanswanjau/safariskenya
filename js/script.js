$(document).ready(function(){


/*
-----------------------------------------------------
MENU STYLING
-----------------------------------------------------
*/


// hide secondary menu & reduce primary menu padding

$('.black').hide();

$(document).scroll(function(){

  if($(this).scrollTop() > 400){

    $('.top-menu').css({'background-color':'#fff', 'box-shadow':'0 0 5px 2px rgba(0, 0, 0, 0.2)'});

    $('.top-menu a').css({'color':'#00cc7a'});

    $('.white').hide();

    $('.black').show();

  }

  if($(this).scrollTop() < 400){

    $('.top-menu').css({'background-color':'transparent', 'box-shadow':'none'});

    $('.top-menu a').css({'color':'#fff'});

    $('.black').hide();

    $('.white').show();

  }

});




// image gallery display

$('.background-holder').hide();


// when close button is clicked

$('.close-it').click(function(){

  $('.background-holder').hide();

});


});
