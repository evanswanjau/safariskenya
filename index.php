<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>This is Africa Limited</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Tours and Travel Agency">
    <link href="https://fonts.googleapis.com/css?family=Palanquin" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <link rel="icon" type="image/png" href="images/logo.png">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>


    <!-- Menu Section -->

    <section>
      <div class="top-menu">
        <ul class="left">
          <a href=""><img class="white" src="images/logo-white.png" alt="logo"><img class="black" src="images/logo.png" alt="logo"></a>
        </ul>
        <ul class="links">
          <a href="destinations"><li>destinations</li></a>
          <a href="hotels"><li>hotels</li></a>
          <a href="offers"><li>offers</li></a>
          <a href="about"><li>about</li></a>
          <a href="contacts"><li>contacts</li></a>
        </ul>
      </div>
    </section>



    <!-- Welcome Section -->

    <section>
      <div class="container-fluid welcome-section">
        <div class="row">
          <div class="text">
            <h1 data-aos='fade-down' data-aos-duration="1000">Travel to any location in the world</h1>
            <form data-aos='fade-up' data-aos-duration="1000" class="search" action="search.php" method="post">
              <input type="text" name="index" placeholder="Where would you like to travel?">
              <button type="submit" name="search" class="right">
                <i class="fa fa-search"></i>
              </button>
            </form>
          </div>
        </div>
      </div>
    </section>



    <!-- Our Style -->

    <section>
      <div class="container-fluid">
        <h2 style="text-align:center; padding-top:5%;">This is Africa Ltd</h2>
        <div class="packages">
          <div class="row">
            <div class="col-sm-3 package">
              <img src="images/plane.png" alt="Perfect Trips">
              <h4><span>BEAUTIFUL DESTINATIONS</span></h4>
              <p>
                Having been in the industry for a long time, we have been able to build relationships throughout the world
                and we can confidently say, that we provide beautiful travel experience to different destinations.
              </p>
            </div>
            <div class="col-sm-3 package">
              <img src="images/hours.png" alt="24 Hour Support">
              <h4><span>24 HOUR SUPPORT</span></h4>
              <p>
                With our active team of experts we are always available to take care of any questions that you may have.
                We will provide information on enquries, package information and any kind of feedback.
              </p>
            </div>
            <div class="col-sm-3 package">
              <img src="images/bagpack.png" alt="Affordable Packages">
              <h4><span>AFFORDABLE PACKAGES</span></h4>
              <p>
                We provide affordable prices for our packages, including our seasonal offers. From high end destinations to simple
                local tourist destinations we have provided the space for you and your family to enjoy.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- Top Travel Locations -->

    <section>
      <div class="container-fluid top-list">
        <h2>FREQUENTLY TRAVELLED</h2>
        <div class="space">
          <div class="row">
            <div class="col-sm-3 visit">
              <div class="capetown">
              </div>
              <br>
              <h4>capetown</h4>
              <p>Considered one of the most beautiful cities in the world. It has a variety of places to visitfrom hiking the beautiful table mountains
              and having a fantastic time at the beach.</p>
              <br>
              <a href="destinations" class="button">view more</a>
            </div>
            <div class="col-sm-3 visit">
              <div class="zanzibar">
              </div>
              <br>
              <h4>zanzibar</h4>
              <p>Zanzibar island has a great reputation on fantastic sites to visit. Rich in Swahili heritage there's no limit to what you can visit or do.
              Including their amazing food.</p>
              <br>
              <a href="destinations" class="button">view more</a>
            </div>
            <div class="col-sm-3 visit">
              <div class="mombasa">
              </div>
              <br>
              <h4>mombasa</h4>
              <p>The second largest city in Kenya after Nairobi. Rich in heritage and culture has beautiful sites including beaches, parks,
              resorts and affortable hotels.</p>
              <br>
              <a href="destinations" class="button">view more</a>
            </div>
          </div>
        </div>
      </div>
    </section>



    <!-- Sites you could visit -->

    <section>
      <div class="container-fluid sites">
        <h2 style="text-align:center;">TOP DESTINATIONS</h2>
        <div class="pad">
          <div class="row">
            <div class="col-sm-5 victoria_falls site">
              <h2><span>VICTORIA FALLS, ZAMBIA AND ZIMBAMBWE</span></h2>
              <p>One of the seven natural wonders of the world, it is the largest waterfall in the world.
                The best part about the location of Victoria Falls is that you can see it from both
                Zimbabwe and Zambia in one day! Start your day in either country and eventually
                make your way over to the other side
                <br><br>
                <a href="contacts" class="button">travel</a>
              </p>
            </div>
            <div class="col-sm-5 pyramids_of_giza site">
              <h2><span>PYRAMIDS OF GIZA, EGYPT</span></h2>
              <p>The iconic symbols of Egypt are the oldest Wonders of the Ancient World, they are massive, impressive, and extremely old.
                To think that they have stood here for centuries is almost unfathomable. For thousands of years, the Great Pyramid of Giza were the tallest structures on the planet.
                <br><br>
                <a href="contacts" class="button">travel</a>
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-5 mountain_gorilla site">
              <h2><span>MOUNTAIN GORILLAS, RWANDA</span></h2>
              <p>The world’s most endangered ape, is found only in small portions of protected afro Montane forests in northwest Rwanda.
                The mountain gorilla is one of many species unique to these forests. The forests are also home to many wonderful birds,
                 primates, large mammals, reptiles, insects, and plants.
                <br><br>
                <a href="contacts" class="button">travel</a>
              </p>
            </div>
            <div class="col-sm-5 flamingo site">
              <h2><span>FLAMINGOS, KENYA</span></h2>
              <p>Protected by Lake Nakuru national Park, the site is a bird watcher’s paradise, home to between 1.5 million and three million flamingos.
                Lake Nakuru is rich in algae, which draws millions of flamingos. Other animals such as warthogs also frequent the lake in search of food.
                <br><br>
                <a href="contatcs" class="button">travel</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>



    <!-- Customer Feedback -->

    <section>
      <div class="container customer">
        <div class="row">
          <div class="col-sm-3 prof">
            <img src="images/prof1.jpg" alt="">
            <br><br>
            <h4>Peter Msalme</h4>
            <p>I had a great time travelling throughout East Africa, we visited the beautiful destinations and I loved every moment of it.</p>
          </div>
          <div class="col-sm-3 prof">
            <img src="images/prof2.jpg" alt="">
            <br><br>
            <h4>Fiona Maggie</h4>
            <p>Mombasa, is an amazing place to visit. I was lucky enought to have chosen This is Africa Ltd. They planned and took care of everything.</p>
          </div>
          <div class="col-sm-3 prof">
            <img src="images/prof3.jpg" alt="">
            <br><br>
            <h4>Fred Wilson</h4>
            <p>I have been a client for the tours and travel agency for three years now. They have never disappointed and I still feel as their number one client.</p>
          </div>
          <div class="col-sm-3 prof">
            <img src="images/prof4.jpg" alt="">
            <br><br>
            <h4>Clare Nandip</h4>
            <p>Having travelled with other agencies before, This is Africa is the number one guide as you travel throughout the planet. Thanks alot guys.</p>
          </div>
        </div>
      </div>
    </section>



    <!-- Footer Section -->

    <div class="container-fluid footer">
      <div class="row">
        <div class="col-sm-4 links">
          <ul>
            <a href="destinations"><li>destinations</li></a>
            <a href="hotels"><li>hotels</li></a>
            <a href="offers"><li>offers</li></a>
            <a href="about"><li>about</li></a>
            <a href="contacts"><li>contacts</li></a>
          </ul>
        </div>
        <div class="col-sm-4 social">
          <ul>
            <a href="#" target="_blank"><li><i class="fab fa-facebook"></i></li></a>
            <a href="#" target="_blank"><li><i class="fab fa-twitter"></i></li></a>
            <a href="#" target="_blank"><li><i class="fab fa-instagram"></i></li></a>
          </ul>
        </div>
        <div class="col-sm-4 subscribe">
          <form class="ui-form" action="" method="post">
            <p>Join our emailing list and be the first to receive our amazing offers</p>
            <input type="email" name="email" placeholder="email"><br>
            <input type="submit" name="subscribe" value="subscribers">
          </form>
        </div>
      </div>
    </div>




  </body>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>AOS.init();</script>
</html>
